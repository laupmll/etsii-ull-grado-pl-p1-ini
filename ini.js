//[~/Dropbox/src/javascript/PLgrado/ini(master)]$ cat ini.js 
"use strict"; // Use ECMAScript 5 strict mode in browsers that support it		//strict

$(document).ready(function() {		// .ready
   $("#fileinput").change(calculate);	//.change
});

function calculate(evt) {
  var f = evt.target.files[0]; 

  if (f) {
    var r = new FileReader();	//FileR
    r.onload = function(e) { 
      var contents = e.target.result;	//	.result
      
      var tokens = lexer(contents);		// Lista de tokens
      var pretty = tokensToString(tokens);
      
      out.className = 'unhidden';		// Cambiar la clase de out
      initialinput.innerHTML = contents;	//.innerHTML
      finaloutput.innerHTML = pretty;		//.
    }
    r.readAsText(f); // Leer como texto		//.readAsText
  } else { 
    alert("Failed to load file");
  }
}

//var temp = '<li> <span class = "nameEqualValue"<%= token.type %>"> <%= match %> </span>\n';		//token.type match (nombres
var temp = '<li> <span class = "<%= token.type %>"<%= token.type %>"> <%= match %> </span>\n';		//token.type match (nombres

function tokensToString(tokens) {
   var r = '';
   for(var i in tokens) {
     var t = tokens[i];
     var s = JSON.stringify(t, undefined, 2);		// Devuelve cadena JSON (pasa el token a cadena) (objeto, atributosAMostrar, númeroDeSangrado(indentado))
     s = _.template(temp, {token: t, match: s});		// Método template de underscore
     r += s;
   }
   return '<ol>\n'+r+'</ol>';
}

function lexer(input) {
  var blanks         = /^\s+/;															// /^__/;
  var iniheader      = /^\[.*?\]/;													// /^__/;
  var comments       = /^;[^\n]*/;													// /^__/;
  var nameEqualValue = /^[a-z]\w*\s*=[^\n]*/i;							// /^__/;
  var any            = /^\S+/;															// /^__/;

  var out = [];
  var m = null;

  while (input != '') {																// Va eliminando lo que va casando
    if (m = blanks.exec(input)) {				//blanks.___		.exec devuelve lo que casa con blanks
		  input = input.substr(m.index + m[0].length);					// index+_____);
			out.push({ type : "blanks" , match: m });							// type: _____, match: _ });
    }
    else if (m = iniheader.exec(input)) {
      input = input.substr(m[0].length);										//.substr(___);
      out.push({ type : "header" , match: m }); 						// avanzemos en input
    }
    else if (m = comments.exec(input)) {										// Si casa con comments,
      input = input.substr(m[0].length);										// .substr(___);
      out.push({ type : "comments" , match: m });						// ___
    }
    else if (m = nameEqualValue.exec(input)) {
      input = input.substr(m[0].length);										// .substr(___);
      out.push({ type : "nameEqualValue" , match: m }); 		// ___
    }
    else if (m = any.exec(input)) {
      out.push({ type : "error" , match: m });							// ___
      input = '';
    }
    else {
      //alert("Fatal Error!"+substr(input,0,20));
      alert("Fatal Error!" + input.substr(0,20));
      input = '';
    }
  }
  return out;
}
